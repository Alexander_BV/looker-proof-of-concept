connection: "oss_san_fernando"
label: "OSS San fernando test"

# include all the views
include: "/views/**/*.view.lkml"

datagroup: oss_san_fernando_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "0 minutes"
}

persist_with: oss_san_fernando_default_datagroup

explore: account_configuration {
  join: users {
    type: left_outer
    sql_on: ${account_configuration.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: delivered_items {
  join: items {
    type: left_outer
    sql_on: ${delivered_items.item_id} = ${items.id} ;;
    relationship: many_to_one
  }

  join: measurement_units {
    type: left_outer
    sql_on: ${items.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }
}

explore: document_types {}

explore: driver_attachments {
  join: drivers {
    type: left_outer
    sql_on: ${driver_attachments.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: attachments {
    type: left_outer
    sql_on: ${driver_attachments.attachment_id} = ${attachments.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${driver_attachments.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${drivers.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }
}

explore: drivers {
  join: providers {
    type: left_outer
    sql_on: ${drivers.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: event_logs {}

explore: events {}

explore: extra_charges {
  join: liquidations {
    type: left_outer
    sql_on: ${extra_charges.liquidation_id} = ${liquidations.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${liquidations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: failed_jobs {}

explore: feature_flags {}

explore: features {
  join: tariffs {
    type: left_outer
    sql_on: ${features.tariff_id} = ${tariffs.id} ;;
    relationship: many_to_one
  }

  join: quotations {
    type: left_outer
    sql_on: ${tariffs.quotation_id} = ${quotations.id} ;;
    relationship: many_to_one
  }
}

explore: filesystem_automation_disk_logs {
  join: filesystem_automation_disks {
    type: left_outer
    sql_on: ${filesystem_automation_disk_logs.filesystem_automation_disk_id} = ${filesystem_automation_disks.id} ;;
    relationship: many_to_one
  }

  join: filesystem_disks {
    type: left_outer
    sql_on: ${filesystem_automation_disks.filesystem_disk_id} = ${filesystem_disks.id} ;;
    relationship: many_to_one
  }

  join: filesystem_automations {
    type: left_outer
    sql_on: ${filesystem_automation_disks.filesystem_automation_id} = ${filesystem_automations.id} ;;
    relationship: many_to_one
  }
}

explore: filesystem_automation_disks {
  join: filesystem_disks {
    type: left_outer
    sql_on: ${filesystem_automation_disks.filesystem_disk_id} = ${filesystem_disks.id} ;;
    relationship: many_to_one
  }

  join: filesystem_automations {
    type: left_outer
    sql_on: ${filesystem_automation_disks.filesystem_automation_id} = ${filesystem_automations.id} ;;
    relationship: many_to_one
  }
}

explore: filesystem_automations {}

explore: filesystem_disks {}

explore: filesystem_events {}

explore: filter_configurations {
  join: users {
    type: left_outer
    sql_on: ${filter_configurations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: general_group {}

explore: general_group_units {
  join: measurement_units {
    type: left_outer
    sql_on: ${general_group_units.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }

  join: general_group {
    type: left_outer
    sql_on: ${general_group_units.general_group_id} = ${general_group.id} ;;
    relationship: many_to_one
  }
}

explore: geocoding_change_history {
  join: geocodings {
    type: left_outer
    sql_on: ${geocoding_change_history.geocoding_id} = ${geocodings.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${geocoding_change_history.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: geocodings {}

explore: historics {}

explore: incidents {
  join: requirements {
    type: left_outer
    sql_on: ${incidents.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${incidents.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${incidents.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${incidents.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${incidents.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: measurement_units {
    type: left_outer
    sql_on: ${incidents.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: interval_transport_status {
  join: trip_intervals {
    type: left_outer
    sql_on: ${interval_transport_status.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${interval_transport_status.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${interval_transport_status.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${trip_intervals.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_intervals.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: invoices {
  join: clients {
    type: left_outer
    sql_on: ${invoices.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${invoices.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: transmitters {
    type: left_outer
    sql_on: ${invoices.transmitter_id} = ${transmitters.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${invoices.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: invoices_orders {
  join: invoices {
    type: left_outer
    sql_on: ${invoices_orders.invoice_id} = ${invoices.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${invoices.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${invoices.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: transmitters {
    type: left_outer
    sql_on: ${invoices.transmitter_id} = ${transmitters.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${invoices.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: item_group_hierarchy {}

explore: item_group_items {
  join: items {
    type: left_outer
    sql_on: ${item_group_items.item_id} = ${items.id} ;;
    relationship: many_to_one
  }

  join: measurement_units {
    type: left_outer
    sql_on: ${items.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }
}

explore: items {
  join: measurement_units {
    type: left_outer
    sql_on: ${items.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }
}

explore: items_group {
  join: general_group {
    type: left_outer
    sql_on: ${items_group.general_group_id} = ${general_group.id} ;;
    relationship: many_to_one
  }
}

explore: items_index {}

explore: items_warehouse {
  join: measurement_units {
    type: left_outer
    sql_on: ${items_warehouse.measurement_unit_id} = ${measurement_units.id} ;;
    relationship: many_to_one
  }

  join: warehouses {
    type: left_outer
    sql_on: ${items_warehouse.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }
}

explore: jobs {}

explore: layouts {}

explore: lead_time_has_vehicle_types {
  join: lead_times {
    type: left_outer
    sql_on: ${lead_time_has_vehicle_types.lead_time_id} = ${lead_times.id} ;;
    relationship: many_to_one
  }
}

explore: lead_times {}

explore: liquidation_details {
  join: liquidations {
    type: left_outer
    sql_on: ${liquidation_details.liquidation_id} = ${liquidations.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${liquidations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: liquidation_trips {
  join: liquidations {
    type: left_outer
    sql_on: ${liquidation_trips.liquidation_id} = ${liquidations.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${liquidation_trips.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${liquidations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }
}

explore: liquidations {
  join: requirements {
    type: left_outer
    sql_on: ${liquidations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: load_types {}

explore: locations {
  join: clients {
    type: left_outer
    sql_on: ${locations.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }
}

explore: login_attempts {}

explore: measurement_units {}

explore: messages {
  join: users {
    type: left_outer
    sql_on: ${messages.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_intervals {
    type: left_outer
    sql_on: ${messages.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${trip_intervals.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }
}

explore: migrations {}

explore: model_has_permissions {
  join: permissions {
    type: left_outer
    sql_on: ${model_has_permissions.permission_id} = ${permissions.id} ;;
    relationship: many_to_one
  }
}

explore: model_has_roles {
  join: roles {
    type: left_outer
    sql_on: ${model_has_roles.role_id} = ${roles.id} ;;
    relationship: many_to_one
  }
}

explore: notifications {}

explore: oauth_access_tokens {
  join: users {
    type: left_outer
    sql_on: ${oauth_access_tokens.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${oauth_access_tokens.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: oauth_auth_codes {
  join: users {
    type: left_outer
    sql_on: ${oauth_auth_codes.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${oauth_auth_codes.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: oauth_clients {
  join: users {
    type: left_outer
    sql_on: ${oauth_clients.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: oauth_personal_access_clients {
  join: clients {
    type: left_outer
    sql_on: ${oauth_personal_access_clients.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }
}

explore: oauth_refresh_tokens {}

explore: operation_entities {
  join: operations {
    type: left_outer
    sql_on: ${operation_entities.operation_id} = ${operations.id} ;;
    relationship: many_to_one
  }
}

explore: operations {}

explore: password_resets {}

explore: paused_events {
  join: trips {
    type: left_outer
    sql_on: ${paused_events.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: payment_has_orders {
  join: payments {
    type: left_outer
    sql_on: ${payment_has_orders.payment_id} = ${payments.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${payments.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${payments.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${payments.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: payments {
  join: trips {
    type: left_outer
    sql_on: ${payments.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${payments.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${payments.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: permissions {}

explore: photos {}

explore: pod {
  join: trips {
    type: left_outer
    sql_on: ${pod.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${pod.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: points {}

explore: polylines {}

explore: postulations {
  join: requirements {
    type: left_outer
    sql_on: ${postulations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${postulations.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${postulations.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${postulations.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trip_proposals.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }
}

explore: properties {}

explore: properties_groups {}

explore: property_options {}

explore: providers {}

explore: quotations {}

explore: rates {}

explore: ratings {}

explore: requirement_item_groups {
  join: requirements {
    type: left_outer
    sql_on: ${requirement_item_groups.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: requirement_items {
  join: requirements {
    type: left_outer
    sql_on: ${requirement_items.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: requirement_item_groups {
    type: left_outer
    sql_on: ${requirement_items.requirement_item_group_id} = ${requirement_item_groups.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${requirement_items.trips_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${requirement_items.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirement_orders {
    type: left_outer
    sql_on: ${requirement_items.requirement_order_id} = ${requirement_orders.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${requirement_orders.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: warehouses {
    type: left_outer
    sql_on: ${requirement_orders.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }
}

explore: requirement_notifications {
  join: requirements {
    type: left_outer
    sql_on: ${requirement_notifications.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: requirement_orders {
  join: clients {
    type: left_outer
    sql_on: ${requirement_orders.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${requirement_orders.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: warehouses {
    type: left_outer
    sql_on: ${requirement_orders.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }
}

explore: requirement_shipment_type {
  join: requirements {
    type: left_outer
    sql_on: ${requirement_shipment_type.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: requirements {
  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: requirements_has_locations {
  join: requirements {
    type: left_outer
    sql_on: ${requirements_has_locations.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${requirements_has_locations.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: tracking {
  join: trip_intervals {
    type: left_outer
    sql_on: ${tracking.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${tracking.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${tracking.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${tracking.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${trip_intervals.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_intervals.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: traffics {}

explore: transmitters {}

explore: transport_containers {
  join: trip_transports {
    type: left_outer
    sql_on: ${transport_containers.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: containers {
    type: left_outer
    sql_on: ${transport_containers.container_id} = ${containers.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${transport_containers.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_intervals {
    type: left_outer
    sql_on: ${transport_containers.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${transport_containers.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${trip_transports.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_transports.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trip_proposals.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }
}

explore: transport_orders {
  join: trip_transports {
    type: left_outer
    sql_on: ${transport_orders.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirement_orders {
    type: left_outer
    sql_on: ${transport_orders.requirement_order_id} = ${requirement_orders.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${transport_orders.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_intervals {
    type: left_outer
    sql_on: ${transport_orders.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${transport_orders.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${trip_transports.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_transports.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trip_proposals.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${requirement_orders.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: warehouses {
    type: left_outer
    sql_on: ${requirement_orders.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }
}

explore: transport_vehicle_containers {
  join: containers {
    type: left_outer
    sql_on: ${transport_vehicle_containers.container_id} = ${containers.id} ;;
    relationship: many_to_one
  }
}

explore: trip_credits {
  join: routes {
    type: left_outer
    sql_on: ${trip_credits.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${trip_credits.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: trip_events {
  join: trips {
    type: left_outer
    sql_on: ${trip_events.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${trip_events.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: trip_intervals {
    type: left_outer
    sql_on: ${trip_events.trip_interval_id} = ${trip_intervals.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${trip_events.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: requirement_items {
    type: left_outer
    sql_on: ${trip_events.requirement_item_id} = ${requirement_items.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }

  join: requirement_item_groups {
    type: left_outer
    sql_on: ${requirement_items.requirement_item_group_id} = ${requirement_item_groups.id} ;;
    relationship: many_to_one
  }

  join: requirement_orders {
    type: left_outer
    sql_on: ${requirement_items.requirement_order_id} = ${requirement_orders.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${requirement_orders.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: warehouses {
    type: left_outer
    sql_on: ${requirement_orders.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }
}

explore: trip_intervals {
  join: trips {
    type: left_outer
    sql_on: ${trip_intervals.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: workflow {
    type: left_outer
    sql_on: ${trip_intervals.workflow_id} = ${workflow.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_intervals.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: trip_proposals {
  join: providers {
    type: left_outer
    sql_on: ${trip_proposals.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trip_proposals.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${trip_proposals.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }
}

explore: trip_transport_compliance_criteria {}

explore: trip_transport_has_drivers {
  join: trips {
    type: left_outer
    sql_on: ${trip_transport_has_drivers.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${trip_transport_has_drivers.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${trip_transport_has_drivers.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: trip_transport_has_vehicles {
  join: vehicles {
    type: left_outer
    sql_on: ${trip_transport_has_vehicles.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: trip_transports {
    type: left_outer
    sql_on: ${trip_transport_has_vehicles.trip_transport_id} = ${trip_transports.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: trips {
    type: left_outer
    sql_on: ${trip_transports.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_transports.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: trip_transports {
  join: trips {
    type: left_outer
    sql_on: ${trip_transports.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: vehicles {
    type: left_outer
    sql_on: ${trip_transports.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${trip_transports.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: postulations {
    type: left_outer
    sql_on: ${trip_transports.postulation_id} = ${postulations.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${trip_transports.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${postulations.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: trip_proposals {
    type: left_outer
    sql_on: ${postulations.trip_proposal_id} = ${trip_proposals.id} ;;
    relationship: many_to_one
  }
}

explore: trips {
  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: trips_services {
  join: trips {
    type: left_outer
    sql_on: ${trips_services.trip_id} = ${trips.id} ;;
    relationship: many_to_one
  }

  join: tariffs {
    type: left_outer
    sql_on: ${trips_services.tariff_id} = ${tariffs.id} ;;
    relationship: many_to_one
  }

  join: requirements {
    type: left_outer
    sql_on: ${trips.requirement_id} = ${requirements.id} ;;
    relationship: many_to_one
  }

  join: routes {
    type: left_outer
    sql_on: ${trips.route_id} = ${routes.id} ;;
    relationship: many_to_one
  }

  join: route_plans {
    type: left_outer
    sql_on: ${trips.route_plan_id} = ${route_plans.id} ;;
    relationship: many_to_one
  }

  join: clients {
    type: left_outer
    sql_on: ${requirements.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${requirements.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: quotations {
    type: left_outer
    sql_on: ${tariffs.quotation_id} = ${quotations.id} ;;
    relationship: many_to_one
  }
}

explore: unit_types {}

explore: users {
  join: clients {
    type: left_outer
    sql_on: ${users.client_id} = ${clients.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${users.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_has_container_types {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_has_container_types.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_has_couplings {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_has_couplings.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_has_load_types {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_has_load_types.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_has_providers {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_has_providers.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicle_has_providers.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_shipment_type {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_shipment_type.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicle_zone {
  join: vehicles {
    type: left_outer
    sql_on: ${vehicle_zone.vehicle_id} = ${vehicles.id} ;;
    relationship: many_to_one
  }

  join: zones {
    type: left_outer
    sql_on: ${vehicle_zone.zone_id} = ${zones.id} ;;
    relationship: many_to_one
  }

  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: vehicles {
  join: providers {
    type: left_outer
    sql_on: ${vehicles.provider_id} = ${providers.id} ;;
    relationship: many_to_one
  }

  join: drivers {
    type: left_outer
    sql_on: ${vehicles.driver_id} = ${drivers.id} ;;
    relationship: many_to_one
  }
}

explore: warehouse_zone {
  join: warehouses {
    type: left_outer
    sql_on: ${warehouse_zone.warehouse_id} = ${warehouses.id} ;;
    relationship: many_to_one
  }

  join: zones {
    type: left_outer
    sql_on: ${warehouse_zone.zone_id} = ${zones.id} ;;
    relationship: many_to_one
  }
}

explore: warehouses {}

explore: webhooks {}

explore: webhooks_logs {}

explore: whatsapp_templates {}

explore: workflow {}

explore: zones {}
