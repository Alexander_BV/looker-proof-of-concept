view: clients {
  sql_table_name: oss_saint_ferdinand_dev.clients ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: document_number {
    type: string
    sql: ${TABLE}.document_number ;;
  }
  dimension: document_type_code {
    type: string
    sql: ${TABLE}.document_type_code ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	name,
	invoices.count,
	locations.count,
	oauth_access_tokens.count,
	oauth_auth_codes.count,
	oauth_personal_access_clients.count,
	requirement_orders.count,
	requirements.count,
	trip_intervals.count,
	trip_transports.count,
	users.count
	]
  }

}
