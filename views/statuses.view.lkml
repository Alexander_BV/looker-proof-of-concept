view: statuses {
  sql_table_name: oss_saint_ferdinand_dev.statuses ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: custom_form_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.custom_form_id ;;
  }
  dimension: is_public {
    type: yesno
    sql: ${TABLE}.is_public ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: next {
    type: string
    sql: ${TABLE}.next ;;
  }
  dimension: order_workflow_id {
    type: number
    sql: ${TABLE}.order_workflow_id ;;
  }
  dimension: required_custom_form {
    type: yesno
    sql: ${TABLE}.required_custom_form ;;
  }
  dimension: required_doc {
    type: yesno
    sql: ${TABLE}.required_doc ;;
  }
  dimension: required_order_form {
    type: yesno
    sql: ${TABLE}.required_order_form ;;
  }
  dimension: required_qualification {
    type: yesno
    sql: ${TABLE}.required_qualification ;;
  }
  dimension: required_signature {
    type: yesno
    sql: ${TABLE}.required_signature ;;
  }
  dimension: required_sub_status {
    type: yesno
    sql: ${TABLE}.required_sub_status ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: workflow_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.workflow_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, name, workflow.id, workflow.name, custom_forms.id]
  }
}
