view: rq_containers {
  sql_table_name: oss_saint_ferdinand_dev.rq_containers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: collection_terminal_id {
    type: number
    sql: ${TABLE}.collection_terminal_id ;;
  }
  dimension: container_type_id {
    type: number
    sql: ${TABLE}.container_type_id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: current_volume {
    type: number
    sql: ${TABLE}.current_volume ;;
  }
  dimension: current_weight {
    type: number
    sql: ${TABLE}.current_weight ;;
  }
  dimension: custody_type_id {
    type: number
    sql: ${TABLE}.custody_type_id ;;
  }
  dimension_group: delivery_date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.delivery_date ;;
  }
  dimension_group: end_load {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.end_load ;;
  }
  dimension: end_location_id {
    type: number
    sql: ${TABLE}.end_location_id ;;
  }
  dimension_group: expected_delivery {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_delivery_date ;;
  }
  dimension_group: expected_return {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_return_date ;;
  }
  dimension_group: expected_withdrawal {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_withdrawal_date ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: has_guard {
    type: yesno
    sql: ${TABLE}.has_guard ;;
  }
  dimension: is_full {
    type: yesno
    sql: ${TABLE}.is_full ;;
  }
  dimension: load_type_id {
    type: number
    sql: ${TABLE}.load_type_id ;;
  }
  dimension: max_volume {
    type: number
    sql: ${TABLE}.max_volume ;;
  }
  dimension: max_weight {
    type: number
    sql: ${TABLE}.max_weight ;;
  }
  dimension: register_user_id {
    type: number
    sql: ${TABLE}.register_user_id ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension_group: return_date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.return_date ;;
  }
  dimension_group: start_load {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.start_load ;;
  }
  dimension: start_location_id {
    type: number
    sql: ${TABLE}.start_location_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension_group: withdrawal_date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.withdrawal_date ;;
  }
  measure: count {
    type: count
    drill_fields: [id, requirements.id]
  }
}
