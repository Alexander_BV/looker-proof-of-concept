view: model_has_roles {
  sql_table_name: oss_saint_ferdinand_dev.model_has_roles ;;

  dimension: model_id {
    type: number
    sql: ${TABLE}.model_id ;;
  }
  dimension: model_type {
    type: string
    sql: ${TABLE}.model_type ;;
  }
  dimension: role_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.role_id ;;
  }
  measure: count {
    type: count
    drill_fields: [roles.id, roles.name, roles.guard_name]
  }
}
