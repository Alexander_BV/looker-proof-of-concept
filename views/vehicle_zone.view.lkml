view: vehicle_zone {
  sql_table_name: oss_saint_ferdinand_dev.vehicle_zone ;;

  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  dimension: zone_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.zone_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id, zones.id, zones.name]
  }
}
