view: tires_rolling {
  sql_table_name: oss_saint_ferdinand_dev.tires_rolling ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: end {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.end_date ;;
  }
  dimension: end_odometer {
    type: number
    sql: ${TABLE}.end_odometer ;;
  }
  dimension: end_rtd {
    type: number
    sql: ${TABLE}.end_rtd ;;
  }
  dimension: retread {
    type: number
    sql: ${TABLE}.retread ;;
  }
  dimension_group: start {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.start_date ;;
  }
  dimension: start_odometer {
    type: number
    sql: ${TABLE}.start_odometer ;;
  }
  dimension: start_rtd {
    type: number
    sql: ${TABLE}.start_rtd ;;
  }
  dimension: tire_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.tire_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, tires.id, vehicles.id]
  }
}
