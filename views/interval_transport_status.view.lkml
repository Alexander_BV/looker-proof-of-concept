view: interval_transport_status {
  sql_table_name: oss_saint_ferdinand_dev.interval_transport_status ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: arrival {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.arrival_date ;;
  }
  dimension_group: cancelled {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.cancelled_date ;;
  }
  dimension: cancelled_motive {
    type: string
    sql: ${TABLE}.cancelled_motive ;;
  }
  dimension: closing_code {
    type: string
    sql: ${TABLE}.closing_code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension_group: end_date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.end_date ;;
  }
  dimension_group: manual_end {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manual_end_at ;;
  }
  dimension: manual_end_motive {
    type: string
    sql: ${TABLE}.manual_end_motive ;;
  }
  dimension: manual_end_user_id {
    type: number
    sql: ${TABLE}.manual_end_user_id ;;
  }
  dimension_group: manual_start {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manual_start_at ;;
  }
  dimension_group: on_my_way {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.on_my_way_date ;;
  }
  dimension_group: start_date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.start_date ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: trip_interval_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_interval_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trip_intervals.id,
	trip_transports.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username
	]
  }

}
