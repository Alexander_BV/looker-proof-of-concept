view: trip_intervals {
  sql_table_name: oss_saint_ferdinand_dev.trip_intervals ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: end_address {
    type: string
    sql: ${TABLE}.end_address ;;
  }
  dimension_group: end {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.end_date ;;
  }
  dimension: end_geohash {
    type: string
    sql: ${TABLE}.end_geohash ;;
  }
  dimension: end_lat {
    type: number
    sql: ${TABLE}.end_lat ;;
  }
  dimension: end_lng {
    type: number
    sql: ${TABLE}.end_lng ;;
  }
  dimension_group: expected_date_finish {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_date_finish ;;
  }
  dimension_group: expected_date_init {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_date_init ;;
  }
  dimension: location_end_id {
    type: number
    sql: ${TABLE}.location_end_id ;;
  }
  dimension: location_start_id {
    type: number
    sql: ${TABLE}.location_start_id ;;
  }
  dimension_group: manipulated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manipulated_at ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: order {
    type: number
    sql: ${TABLE}.`order` ;;
  }
  dimension: service_time {
    type: number
    sql: ${TABLE}.service_time ;;
  }
  dimension: start_address {
    type: string
    sql: ${TABLE}.start_address ;;
  }
  dimension_group: start {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.start_date ;;
  }
  dimension: start_geohash {
    type: string
    sql: ${TABLE}.start_geohash ;;
  }
  dimension: start_lat {
    type: number
    sql: ${TABLE}.start_lat ;;
  }
  dimension: start_lng {
    type: number
    sql: ${TABLE}.start_lng ;;
  }
  dimension: status_all {
    type: string
    sql: ${TABLE}.status_all ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_manipulated_id {
    type: number
    sql: ${TABLE}.user_manipulated_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: workflow_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.workflow_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trips.id,
	workflow.id,
	workflow.name,
	clients.id,
	clients.name,
	interval_transport_status.count,
	messages.count,
	tracking.count,
	transport_containers.count,
	transport_orders.count,
	trip_events.count
	]
  }

}
