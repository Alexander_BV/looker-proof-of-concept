view: rates {
  sql_table_name: oss_saint_ferdinand_dev.rates ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: base_km {
    type: number
    sql: ${TABLE}.base_km ;;
  }
  dimension: base_price_km {
    type: number
    sql: ${TABLE}.base_price_km ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: max_time_km {
    type: number
    sql: ${TABLE}.max_time_km ;;
  }
  dimension: price_extra_time {
    type: number
    sql: ${TABLE}.price_extra_time ;;
  }
  dimension: price_km {
    type: number
    sql: ${TABLE}.price_km ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
