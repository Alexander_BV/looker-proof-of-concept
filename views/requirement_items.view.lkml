view: requirement_items {
  sql_table_name: oss_saint_ferdinand_dev.requirement_items ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: load_type_code {
    type: string
    sql: ${TABLE}.load_type_code ;;
  }
  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension: requirement_item_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_item_group_id ;;
  }
  dimension: requirement_order_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_order_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension: trips_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trips_id ;;
  }
  dimension: unit_type_code {
    type: string
    sql: ${TABLE}.unit_type_code ;;
  }
  dimension: unit_weight {
    type: number
    sql: ${TABLE}.unit_weight ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	requirements.id,
	requirement_item_groups.id,
	trips.id,
	trip_transports.id,
	requirement_orders.id,
	requirement_orders.client_name,
	trip_events.count
	]
  }

}
