view: trip_events {
  sql_table_name: oss_saint_ferdinand_dev.trip_events ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: event_code {
    type: string
    sql: ${TABLE}.event_code ;;
  }
  dimension_group: event {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.event_date ;;
  }
  dimension: geo_lat {
    type: number
    sql: ${TABLE}.geo_lat ;;
  }
  dimension: geo_lng {
    type: number
    sql: ${TABLE}.geo_lng ;;
  }
  dimension: hash {
    type: string
    sql: ${TABLE}.hash ;;
  }
  dimension: requirement_item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_item_id ;;
  }
  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_interval_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_interval_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trips.id,
	trip_transports.id,
	trip_intervals.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	requirement_items.id
	]
  }

}
