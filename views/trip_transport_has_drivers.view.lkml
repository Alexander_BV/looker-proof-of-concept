view: trip_transport_has_drivers {
  sql_table_name: oss_saint_ferdinand_dev.trip_transport_has_drivers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trips.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	trip_transports.id
	]
  }

}
