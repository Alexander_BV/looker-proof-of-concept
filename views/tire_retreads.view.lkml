view: tire_retreads {
  sql_table_name: oss_saint_ferdinand_dev.tire_retreads ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: cost {
    type: number
    sql: ${TABLE}.cost ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: retread {
    type: number
    sql: ${TABLE}.retread ;;
  }
  dimension: rtd {
    type: number
    sql: ${TABLE}.rtd ;;
  }
  dimension: tire_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.tire_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, tires.id, providers.id, providers.name]
  }
}
