view: vehicle_has_providers {
  sql_table_name: oss_saint_ferdinand_dev.vehicle_has_providers ;;

  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id, providers.id, providers.name]
  }
}
