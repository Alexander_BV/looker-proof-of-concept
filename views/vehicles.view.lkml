view: vehicles {
  sql_table_name: oss_saint_ferdinand_dev.vehicles ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: axes_number {
    type: number
    sql: ${TABLE}.axes_number ;;
  }
  dimension: cellphone_gps {
    type: yesno
    sql: ${TABLE}.cellphone_gps ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: has_motor {
    type: yesno
    sql: ${TABLE}.has_motor ;;
  }
  dimension: is_load_type {
    type: yesno
    sql: ${TABLE}.is_load_type ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: max_volume {
    type: number
    sql: ${TABLE}.max_volume ;;
  }
  dimension: max_weight {
    type: number
    sql: ${TABLE}.max_weight ;;
  }
  dimension: plate {
    type: string
    sql: ${TABLE}.plate ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: tires_number {
    type: number
    sql: ${TABLE}.tires_number ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: vehicle_type_id {
    type: number
    sql: ${TABLE}.vehicle_type_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	providers.id,
	providers.name,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	incidents.count,
	postulations.count,
	subsidiary_vehicle.count,
	tires.count,
	tires_rolling.count,
	trip_transport_has_vehicles.count,
	trip_transports.count,
	vehicle_has_container_types.count,
	vehicle_has_couplings.count,
	vehicle_has_load_types.count,
	vehicle_has_providers.count,
	vehicle_shipment_type.count,
	vehicle_zone.count
	]
  }

}
