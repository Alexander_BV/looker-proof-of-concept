view: containers {
  sql_table_name: oss_saint_ferdinand_dev.containers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: cubic_capacity {
    type: number
    sql: ${TABLE}.cubic_capacity ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }
  dimension: length {
    type: number
    sql: ${TABLE}.length ;;
  }
  dimension: payload {
    type: number
    sql: ${TABLE}.payload ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }
  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	container_history.count,
	container_load_types.count,
	containers_load.count,
	transport_containers.count,
	transport_vehicle_containers.count
	]
  }

}
