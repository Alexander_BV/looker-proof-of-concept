view: requirement_item_groups {
  sql_table_name: oss_saint_ferdinand_dev.requirement_item_groups ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: container_number {
    type: string
    sql: ${TABLE}.container_number ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }
  dimension: is_container {
    type: yesno
    sql: ${TABLE}.is_container ;;
  }
  dimension: length {
    type: number
    sql: ${TABLE}.length ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }
  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }
  measure: count {
    type: count
    drill_fields: [id, requirements.id, requirement_items.count]
  }
}
