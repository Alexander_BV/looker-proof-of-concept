view: requirements_has_locations {
  sql_table_name: oss_saint_ferdinand_dev.requirements_has_locations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: expected_date_finish {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expected_date_finish ;;
  }
  dimension: geo_lat {
    type: number
    sql: ${TABLE}.geo_lat ;;
  }
  dimension: geo_lng {
    type: number
    sql: ${TABLE}.geo_lng ;;
  }
  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }
  dimension: mode {
    type: string
    sql: ${TABLE}.mode ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id, requirements.id, locations.id, locations.name]
  }
}
