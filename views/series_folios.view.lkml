view: series_folios {
  sql_table_name: oss_saint_ferdinand_dev.series_folios ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: document_type {
    type: string
    sql: ${TABLE}.document_type ;;
  }
  dimension: folio {
    type: string
    sql: ${TABLE}.folio ;;
  }
  dimension: serie {
    type: string
    sql: ${TABLE}.serie ;;
  }
  dimension: transmitter_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.transmitter_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, transmitters.id, transmitters.business_name]
  }
}
