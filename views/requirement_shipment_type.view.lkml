view: requirement_shipment_type {
  sql_table_name: oss_saint_ferdinand_dev.requirement_shipment_type ;;

  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension: shipment_type_id {
    type: number
    sql: ${TABLE}.shipment_type_id ;;
  }
  measure: count {
    type: count
    drill_fields: [requirements.id]
  }
}
