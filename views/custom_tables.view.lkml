view: custom_tables {
  sql_table_name: oss_saint_ferdinand_dev.custom_tables ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: columns {
    type: string
    sql: ${TABLE}.columns ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }
  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
