view: quotations {
  sql_table_name: oss_saint_ferdinand_dev.quotations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: apply_for {
    type: string
    sql: ${TABLE}.apply_for ;;
  }
  dimension_group: available {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.available_at ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: file_path {
    type: string
    sql: ${TABLE}.file_path ;;
  }
  dimension: is_active {
    type: yesno
    sql: ${TABLE}.is_active ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: quotable_id {
    type: number
    sql: ${TABLE}.quotable_id ;;
  }
  dimension: quotable_type {
    type: string
    sql: ${TABLE}.quotable_type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, name, tariffs.count]
  }
}
