view: event_logs {
  sql_table_name: oss_saint_ferdinand_dev.event_logs ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: detail {
    type: string
    sql: ${TABLE}.detail ;;
  }
  dimension_group: device_timestamp {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.device_timestamp ;;
  }
  dimension: event {
    type: string
    sql: ${TABLE}.event ;;
  }
  dimension_group: expiration {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expiration_date ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: hash {
    type: string
    sql: ${TABLE}.hash ;;
  }
  dimension: identifier {
    type: string
    sql: ${TABLE}.identifier ;;
  }
  dimension: is_dismissed {
    type: yesno
    sql: ${TABLE}.is_dismissed ;;
  }
  dimension: is_offline {
    type: yesno
    sql: ${TABLE}.is_offline ;;
  }
  dimension: is_paused {
    type: yesno
    sql: ${TABLE}.is_paused ;;
  }
  dimension_group: manipulated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manipulated_at ;;
  }
  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }
  dimension: omit_in_route_compliance {
    type: yesno
    sql: ${TABLE}.omit_in_route_compliance ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_manipulated_id {
    type: number
    sql: ${TABLE}.user_manipulated_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
