view: model_has_permissions {
  sql_table_name: oss_saint_ferdinand_dev.model_has_permissions ;;

  dimension: model_id {
    type: number
    sql: ${TABLE}.model_id ;;
  }
  dimension: model_type {
    type: string
    sql: ${TABLE}.model_type ;;
  }
  dimension: permission_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.permission_id ;;
  }
  measure: count {
    type: count
    drill_fields: [permissions.id, permissions.name, permissions.guard_name]
  }
}
