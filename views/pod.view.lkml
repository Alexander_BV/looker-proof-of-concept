view: pod {
  sql_table_name: oss_saint_ferdinand_dev.pod ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_user_id {
    type: number
    sql: ${TABLE}.creator_user_id ;;
  }
  dimension: creator_user_type {
    type: string
    sql: ${TABLE}.creator_user_type ;;
  }
  dimension: entity_id {
    type: number
    sql: ${TABLE}.entity_id ;;
  }
  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_validator_id {
    type: number
    sql: ${TABLE}.user_validator_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension_group: validated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.validated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id, trips.id, trip_transports.id]
  }
}
