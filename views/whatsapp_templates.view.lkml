view: whatsapp_templates {
  sql_table_name: oss_saint_ferdinand_dev.whatsapp_templates ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: event {
    type: string
    sql: ${TABLE}.event ;;
  }
  dimension: template {
    type: string
    sql: ${TABLE}.template ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
