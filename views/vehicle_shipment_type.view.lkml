view: vehicle_shipment_type {
  sql_table_name: oss_saint_ferdinand_dev.vehicle_shipment_type ;;

  dimension: shipment_type_id {
    type: number
    sql: ${TABLE}.shipment_type_id ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id]
  }
}
