view: warehouse_zone {
  sql_table_name: oss_saint_ferdinand_dev.warehouse_zone ;;

  dimension: warehouse_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.warehouse_id ;;
  }
  dimension: zone_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.zone_id ;;
  }
  measure: count {
    type: count
    drill_fields: [warehouses.id, warehouses.contact_name, zones.id, zones.name]
  }
}
