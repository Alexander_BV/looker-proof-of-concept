view: requirement_orders {
  sql_table_name: oss_saint_ferdinand_dev.requirement_orders ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: assigned {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.assigned_at ;;
  }
  dimension_group: assignment_attempt {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.assignment_attempt_at ;;
  }
  dimension: attempts {
    type: number
    sql: ${TABLE}.attempts ;;
  }
  dimension: client_address {
    type: string
    sql: ${TABLE}.client_address ;;
  }
  dimension: client_document {
    type: string
    sql: ${TABLE}.client_document ;;
  }
  dimension: client_email {
    type: string
    sql: ${TABLE}.client_email ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension: client_lat {
    type: number
    sql: ${TABLE}.client_lat ;;
  }
  dimension: client_lng {
    type: number
    sql: ${TABLE}.client_lng ;;
  }
  dimension: client_name {
    type: string
    sql: ${TABLE}.client_name ;;
  }
  dimension: client_phone {
    type: string
    sql: ${TABLE}.client_phone ;;
  }
  dimension: closing_code {
    type: string
    sql: ${TABLE}.closing_code ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: deleted {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.deleted_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: is_chargeable {
    type: yesno
    sql: ${TABLE}.is_chargeable ;;
  }
  dimension: is_overtime_notify {
    type: yesno
    sql: ${TABLE}.is_overtime_notify ;;
  }
  dimension: is_visible {
    type: yesno
    sql: ${TABLE}.is_visible ;;
  }
  dimension: last_mile {
    type: yesno
    sql: ${TABLE}.last_mile ;;
  }
  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }
  dimension: location_start_id {
    type: number
    sql: ${TABLE}.location_start_id ;;
  }
  dimension_group: manipulated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manipulated_at ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }
  dimension: order_clone_id {
    type: number
    sql: ${TABLE}.order_clone_id ;;
  }
  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }
  dimension_group: rejected {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.rejected_at ;;
  }
  dimension: service_time {
    type: number
    sql: ${TABLE}.service_time ;;
  }
  dimension: start_address {
    type: string
    sql: ${TABLE}.start_address ;;
  }
  dimension: start_lat {
    type: number
    sql: ${TABLE}.start_lat ;;
  }
  dimension: start_lng {
    type: number
    sql: ${TABLE}.start_lng ;;
  }
  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }
  dimension: time_window {
    type: string
    sql: ${TABLE}.time_window ;;
  }
  dimension: tracking_number {
    type: string
    sql: ${TABLE}.tracking_number ;;
  }
  dimension: unassignable_reason {
    type: string
    sql: ${TABLE}.unassignable_reason ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_manipulated_id {
    type: number
    sql: ${TABLE}.user_manipulated_id ;;
  }
  dimension: user_responsible_id {
    type: number
    sql: ${TABLE}.user_responsible_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: volume {
    type: number
    sql: ${TABLE}.volume ;;
  }
  dimension: warehouse_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.warehouse_id ;;
  }
  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	client_name,
	clients.id,
	clients.name,
	locations.id,
	locations.name,
	warehouses.id,
	warehouses.contact_name,
	requirement_items.count,
	transport_orders.count
	]
  }

}
