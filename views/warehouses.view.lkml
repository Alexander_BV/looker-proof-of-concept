view: warehouses {
  sql_table_name: oss_saint_ferdinand_dev.warehouses ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: contact_name {
    type: string
    sql: ${TABLE}.contact_name ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: locatable_id {
    type: number
    sql: ${TABLE}.locatable_id ;;
  }
  dimension: locatable_type {
    type: string
    sql: ${TABLE}.locatable_type ;;
  }
  dimension: subsidiary_id {
    type: number
    sql: ${TABLE}.subsidiary_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, contact_name, items_warehouse.count, requirement_orders.count, warehouse_zone.count]
  }
}
