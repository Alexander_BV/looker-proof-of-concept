view: lead_times {
  sql_table_name: oss_saint_ferdinand_dev.lead_times ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: coordinates_end {
    type: string
    sql: ${TABLE}.coordinates_end ;;
  }
  dimension: coordinates_start {
    type: string
    sql: ${TABLE}.coordinates_start ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: end_type {
    type: string
    sql: ${TABLE}.end_type ;;
  }
  dimension: location_end_id {
    type: number
    sql: ${TABLE}.location_end_id ;;
  }
  dimension: location_start_id {
    type: number
    sql: ${TABLE}.location_start_id ;;
  }
  dimension: minutes {
    type: number
    sql: ${TABLE}.minutes ;;
  }
  dimension: start_type {
    type: string
    sql: ${TABLE}.start_type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: zone_end_id {
    type: number
    sql: ${TABLE}.zone_end_id ;;
  }
  dimension: zone_start_id {
    type: number
    sql: ${TABLE}.zone_start_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, lead_time_has_vehicle_types.count]
  }
}
