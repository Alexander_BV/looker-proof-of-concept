view: vehicle_has_container_types {
  sql_table_name: oss_saint_ferdinand_dev.vehicle_has_container_types ;;

  dimension: container_type_id {
    type: number
    sql: ${TABLE}.container_type_id ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id]
  }
}
