view: spents {
  sql_table_name: oss_saint_ferdinand_dev.spents ;;
  drill_fields: [reason_rejection_spent_id]

  dimension: reason_rejection_spent_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.reason_rejection_spent_id ;;
  }
  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }
  dimension: assumed_by_client {
    type: number
    sql: ${TABLE}.assumed_by_client ;;
  }
  dimension: assumed_by_client_is_percentage {
    type: yesno
    sql: ${TABLE}.assumed_by_client_is_percentage ;;
  }
  dimension: assumed_by_provider {
    type: number
    sql: ${TABLE}.assumed_by_provider ;;
  }
  dimension: assumed_by_provider_is_percentage {
    type: yesno
    sql: ${TABLE}.assumed_by_provider_is_percentage ;;
  }
  dimension: avz_uuid {
    type: string
    sql: ${TABLE}.avz_uuid ;;
  }
  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_user_id {
    type: number
    sql: ${TABLE}.creator_user_id ;;
  }
  dimension: creator_user_type {
    type: string
    sql: ${TABLE}.creator_user_type ;;
  }
  dimension: currency_id {
    type: number
    sql: ${TABLE}.currency_id ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: spent_type_id {
    type: number
    sql: ${TABLE}.spent_type_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_validator_id {
    type: number
    sql: ${TABLE}.user_validator_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension_group: validated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.validated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	reason_rejection_spent_id,
	trips.id,
	trip_transports.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username
	]
  }

}
