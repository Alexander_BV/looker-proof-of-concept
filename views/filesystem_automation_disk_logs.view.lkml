view: filesystem_automation_disk_logs {
  sql_table_name: oss_saint_ferdinand_dev.filesystem_automation_disk_logs ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: exception {
    type: string
    sql: ${TABLE}.exception ;;
  }
  dimension: filesystem_automation_disk_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.filesystem_automation_disk_id ;;
  }
  dimension: http_code {
    type: number
    sql: ${TABLE}.http_code ;;
  }
  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }
  dimension: path_target {
    type: string
    sql: ${TABLE}.path_target ;;
  }
  dimension: payload {
    type: string
    sql: ${TABLE}.payload ;;
  }
  dimension: queue {
    type: string
    sql: ${TABLE}.queue ;;
  }
  dimension: summary {
    type: string
    sql: ${TABLE}.summary ;;
  }
  measure: count {
    type: count
    drill_fields: [id, filesystem_automation_disks.id, filesystem_automation_disks.template_filename]
  }
}
