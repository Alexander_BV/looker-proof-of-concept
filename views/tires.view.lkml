view: tires {
  sql_table_name: oss_saint_ferdinand_dev.tires ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: code_reason_dropping {
    type: string
    sql: ${TABLE}.code_reason_dropping ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: design {
    type: string
    sql: ${TABLE}.design ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: measure {
    type: string
    sql: ${TABLE}.measure ;;
  }
  dimension: original_rtd {
    type: number
    sql: ${TABLE}.original_rtd ;;
  }
  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }
  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }
  dimension: retread {
    type: number
    sql: ${TABLE}.retread ;;
  }
  dimension: retread_band_design {
    type: string
    sql: ${TABLE}.retread_band_design ;;
  }
  dimension: retread_rtd {
    type: number
    sql: ${TABLE}.retread_rtd ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	vehicles.id,
	users.id,
	users.first_name,
	users.last_name,
	tire_retreads.count,
	tires_history.count,
	tires_rolling.count
	]
  }

}
