view: extra_charges {
  sql_table_name: oss_saint_ferdinand_dev.extra_charges ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: extra_charge_reason_id {
    type: number
    sql: ${TABLE}.extra_charge_reason_id ;;
  }
  dimension: is_incremental {
    type: yesno
    sql: ${TABLE}.is_incremental ;;
  }
  dimension: liquidation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.liquidation_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, liquidations.id]
  }
}
