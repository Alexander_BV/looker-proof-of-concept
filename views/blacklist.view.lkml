view: blacklist {
  sql_table_name: oss_saint_ferdinand_dev.blacklist ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: close_reason {
    type: string
    sql: ${TABLE}.close_reason ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: rejectable_id {
    type: number
    sql: ${TABLE}.rejectable_id ;;
  }
  dimension: rejectable_type {
    type: string
    sql: ${TABLE}.rejectable_type ;;
  }
  dimension: rejecter_user_id {
    type: number
    sql: ${TABLE}.rejecter_user_id ;;
  }
  dimension: source_id {
    type: number
    sql: ${TABLE}.source_id ;;
  }
  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
