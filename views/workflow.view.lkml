view: workflow {
  sql_table_name: oss_saint_ferdinand_dev.workflow ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: init_status {
    type: number
    sql: ${TABLE}.init_status ;;
  }
  dimension: is_public {
    type: yesno
    sql: ${TABLE}.is_public ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, name, statuses.count, trip_intervals.count]
  }
}
