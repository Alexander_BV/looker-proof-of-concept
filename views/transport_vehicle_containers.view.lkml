view: transport_vehicle_containers {
  sql_table_name: oss_saint_ferdinand_dev.transport_vehicle_containers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: container_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.container_id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: transport_vehicle_id {
    type: number
    sql: ${TABLE}.transport_vehicle_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id, containers.id]
  }
}
