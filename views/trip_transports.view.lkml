view: trip_transports {
  sql_table_name: oss_saint_ferdinand_dev.trip_transports ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: cancel_motive {
    type: string
    sql: ${TABLE}.cancel_motive ;;
  }
  dimension_group: cancelled {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.cancelled_at ;;
  }
  dimension_group: change {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.change_date ;;
  }
  dimension: change_motive {
    type: string
    sql: ${TABLE}.change_motive ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension: closing_code {
    type: string
    sql: ${TABLE}.closing_code ;;
  }
  dimension_group: created_at {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: current_driver_id {
    type: number
    sql: ${TABLE}.current_driver_id ;;
  }
  dimension_group: events_report_rpd_created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.events_report_rpd_created_at ;;
  }
  dimension_group: events_report_rpd_updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.events_report_rpd_updated_at ;;
  }
  dimension: events_report_rpd_uuid {
    type: string
    sql: ${TABLE}.events_report_rpd_uuid ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: is_complete {
    type: yesno
    sql: ${TABLE}.is_complete ;;
  }
  dimension: is_read_only {
    type: yesno
    sql: ${TABLE}.is_read_only ;;
  }
  dimension: is_visible_on_portal {
    type: yesno
    sql: ${TABLE}.is_visible_on_portal ;;
  }
  dimension: last_geofence {
    type: string
    sql: ${TABLE}.last_geofence ;;
  }
  dimension: lmm_uuid {
    type: string
    sql: ${TABLE}.lmm_uuid ;;
  }
  dimension_group: manipulated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.manipulated_at ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: postulation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.postulation_id ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: route_compliance_percentage {
    type: number
    sql: ${TABLE}.route_compliance_percentage ;;
  }
  dimension_group: route_plan_detector_updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.route_plan_detector_updated_at ;;
  }
  dimension: route_plan_is_active {
    type: yesno
    sql: ${TABLE}.route_plan_is_active ;;
  }
  dimension: transport_from_id {
    type: number
    sql: ${TABLE}.transport_from_id ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_status {
    type: string
    sql: ${TABLE}.trip_status ;;
  }
  dimension_group: updated_at {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_manipulated_id {
    type: number
    sql: ${TABLE}.user_manipulated_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: uuid_route_plan_detector {
    type: string
    sql: ${TABLE}.uuid_route_plan_detector ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  dimension: volume {
    type: number
    sql: ${TABLE}.volume ;;
  }
  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trips.id,
	vehicles.id,
	providers.id,
	providers.name,
	postulations.id,
	clients.id,
	clients.name,
	container_history.count,
	containers_load.count,
	incidents.count,
	interval_transport_status.count,
	invoices.count,
	pod.count,
	requirement_items.count,
	shared_documents.count,
	spents.count,
	tracking.count,
	transport_containers.count,
	transport_orders.count,
	trip_events.count,
	trip_transport_has_drivers.count,
	trip_transport_has_vehicles.count
	]
  }

}
