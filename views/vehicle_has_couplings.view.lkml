view: vehicle_has_couplings {
  sql_table_name: oss_saint_ferdinand_dev.vehicle_has_couplings ;;

  dimension: coupling_id {
    type: number
    sql: ${TABLE}.coupling_id ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id]
  }
}
