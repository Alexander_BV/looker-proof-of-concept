view: subsidiary_vehicle {
  sql_table_name: oss_saint_ferdinand_dev.subsidiary_vehicle ;;

  dimension: subsidiary_id {
    type: number
    sql: ${TABLE}.subsidiary_id ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [vehicles.id]
  }
}
