view: telescope_entries_tags {
  sql_table_name: oss_saint_ferdinand_dev.telescope_entries_tags ;;

  dimension: entry_uuid {
    type: string
    sql: ${TABLE}.entry_uuid ;;
  }
  dimension: tag {
    type: string
    sql: ${TABLE}.tag ;;
  }
  measure: count {
    type: count
  }
}
