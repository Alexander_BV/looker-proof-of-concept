view: taggables {
  sql_table_name: oss_saint_ferdinand_dev.taggables ;;
  drill_fields: [taggable_id]

  dimension: taggable_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taggable_id ;;
  }
  dimension: tag_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.tag_id ;;
  }
  dimension: taggable_type {
    type: string
    sql: ${TABLE}.taggable_type ;;
  }
  measure: count {
    type: count
    drill_fields: [taggable_id, tags.id, tags.name]
  }
}
