view: subsidiary_driver {
  sql_table_name: oss_saint_ferdinand_dev.subsidiary_driver ;;

  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: subsidiary_id {
    type: number
    sql: ${TABLE}.subsidiary_id ;;
  }
  measure: count {
    type: count
    drill_fields: [drivers.id, drivers.first_name, drivers.last_name, drivers.username]
  }
}
