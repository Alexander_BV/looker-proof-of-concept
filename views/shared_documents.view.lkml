view: shared_documents {
  sql_table_name: oss_saint_ferdinand_dev.shared_documents ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: attachment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.attachment_id ;;
  }
  dimension: avz_uuid {
    type: string
    sql: ${TABLE}.avz_uuid ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: entity_data {
    type: string
    sql: ${TABLE}.entity_data ;;
  }
  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }
  dimension: is_visible {
    type: yesno
    sql: ${TABLE}.is_visible ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, trips.id, trip_transports.id, attachments.id, attachments.original_name]
  }
}
