view: filter_configurations {
  sql_table_name: oss_saint_ferdinand_dev.filter_configurations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code_filter_form {
    type: string
    sql: ${TABLE}.code_filter_form ;;
  }
  dimension: configuration {
    type: string
    sql: ${TABLE}.configuration ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, users.id, users.first_name, users.last_name]
  }
}
