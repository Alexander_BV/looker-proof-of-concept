view: locations {
  sql_table_name: oss_saint_ferdinand_dev.locations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }
  dimension: area_type {
    type: string
    sql: ${TABLE}.area_type ;;
  }
  dimension: checkpoint_uuid {
    type: string
    sql: ${TABLE}.checkpoint_uuid ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: geo_lat {
    type: number
    sql: ${TABLE}.geo_lat ;;
  }
  dimension: geo_lng {
    type: number
    sql: ${TABLE}.geo_lng ;;
  }
  dimension: gmap_zoom {
    type: number
    sql: ${TABLE}.gmap_zoom ;;
  }
  dimension: is_checkpoint {
    type: yesno
    sql: ${TABLE}.is_checkpoint ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: radius {
    type: number
    sql: ${TABLE}.radius ;;
  }
  dimension: service_time {
    type: number
    sql: ${TABLE}.service_time ;;
  }
  dimension: time_type {
    type: string
    sql: ${TABLE}.time_type ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	name,
	clients.id,
	clients.name,
	requirement_orders.count,
	requirements_has_locations.count,
	tracking.count
	]
  }

}
