view: route_plan_stops {
  sql_table_name: oss_saint_ferdinand_dev.route_plan_stops ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: extension {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.extension_time ;;
  }
  dimension: is_required {
    type: yesno
    sql: ${TABLE}.is_required ;;
  }
  dimension: lat {
    type: number
    sql: ${TABLE}.lat ;;
  }
  dimension: lng {
    type: number
    sql: ${TABLE}.lng ;;
  }
  dimension: radio {
    type: number
    sql: ${TABLE}.radio ;;
  }
  dimension: route_plan_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.route_plan_id ;;
  }
  dimension: route_stop_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.route_stop_id ;;
  }
  dimension_group: stopped {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.stopped_time ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, route_plans.id, route_plans.name, route_stops.id, route_stops.name]
  }
}
