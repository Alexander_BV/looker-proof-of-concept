view: requirements {
  sql_table_name: oss_saint_ferdinand_dev.requirements ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: approval_user_id {
    type: number
    sql: ${TABLE}.approval_user_id ;;
  }
  dimension_group: approved {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.approved_at ;;
  }
  dimension_group: cancelled {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.cancelled_at ;;
  }
  dimension: cancelled_motive {
    type: string
    sql: ${TABLE}.cancelled_motive ;;
  }
  dimension: canceller_user_id {
    type: number
    sql: ${TABLE}.canceller_user_id ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }
  dimension: container_type_id {
    type: number
    sql: ${TABLE}.container_type_id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_user_id {
    type: number
    sql: ${TABLE}.creator_user_id ;;
  }
  dimension: destinations {
    type: string
    sql: ${TABLE}.destinations ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: load_type_id {
    type: number
    sql: ${TABLE}.load_type_id ;;
  }
  dimension: load_weight {
    type: number
    sql: ${TABLE}.load_weight ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: origins {
    type: string
    sql: ${TABLE}.origins ;;
  }
  dimension: packing_id {
    type: number
    sql: ${TABLE}.packing_id ;;
  }
  dimension: port_id {
    type: number
    sql: ${TABLE}.port_id ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }
  dimension_group: rejected {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.rejected_at ;;
  }
  dimension: rejected_motive {
    type: string
    sql: ${TABLE}.rejected_motive ;;
  }
  dimension: rejecter_user_id {
    type: number
    sql: ${TABLE}.rejecter_user_id ;;
  }
  dimension_group: requested {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.requested_at ;;
  }
  dimension: requirement_type {
    type: string
    sql: ${TABLE}.requirement_type ;;
  }
  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }
  dimension: shipping_line_id {
    type: number
    sql: ${TABLE}.shipping_line_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: total_containers {
    type: number
    sql: ${TABLE}.total_containers ;;
  }
  dimension: transport_info {
    type: string
    sql: ${TABLE}.transport_info ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	clients.id,
	clients.name,
	providers.id,
	providers.name,
	incidents.count,
	liquidations.count,
	postulations.count,
	requirement_item_groups.count,
	requirement_items.count,
	requirement_notifications.count,
	requirement_shipment_type.count,
	requirements_has_locations.count,
	rq_containers.count,
	transport_containers.count,
	transport_orders.count,
	trips.count
	]
  }

}
