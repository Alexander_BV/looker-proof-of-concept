view: route_plan_sections {
  sql_table_name: oss_saint_ferdinand_dev.route_plan_sections ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: extension {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.extension_time ;;
  }
  dimension: order {
    type: number
    sql: ${TABLE}.`order` ;;
  }
  dimension: points {
    type: string
    sql: ${TABLE}.points ;;
  }
  dimension: radio {
    type: number
    sql: ${TABLE}.radio ;;
  }
  dimension: route_plan_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.route_plan_id ;;
  }
  dimension_group: time {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.time ;;
  }
  dimension: total_distance {
    type: number
    sql: ${TABLE}.total_distance ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: velocity {
    type: number
    sql: ${TABLE}.velocity ;;
  }
  measure: count {
    type: count
    drill_fields: [id, route_plans.id, route_plans.name]
  }
}
