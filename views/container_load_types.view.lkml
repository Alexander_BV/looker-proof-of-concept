view: container_load_types {
  sql_table_name: oss_saint_ferdinand_dev.container_load_types ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: containers_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.containers_id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: load_type_code {
    type: string
    sql: ${TABLE}.load_type_code ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id, containers.id]
  }
}
