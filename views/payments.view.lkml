view: payments {
  sql_table_name: oss_saint_ferdinand_dev.payments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: agency_id {
    type: number
    sql: ${TABLE}.agency_id ;;
  }
  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }
  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: currency_id {
    type: number
    sql: ${TABLE}.currency_id ;;
  }
  dimension_group: date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.date ;;
  }
  dimension: destination_account_id {
    type: number
    sql: ${TABLE}.destination_account_id ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: operation_code {
    type: string
    sql: ${TABLE}.operation_code ;;
  }
  dimension: payment_type_id {
    type: number
    sql: ${TABLE}.payment_type_id ;;
  }
  dimension: rejection_reason {
    type: string
    sql: ${TABLE}.rejection_reason ;;
  }
  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }
  dimension: transaction_code {
    type: string
    sql: ${TABLE}.transaction_code ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trips.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	users.id,
	users.first_name,
	users.last_name,
	payment_has_orders.count
	]
  }

}
