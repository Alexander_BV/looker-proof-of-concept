view: telescope_entries {
  sql_table_name: oss_saint_ferdinand_dev.telescope_entries ;;

  dimension: batch_id {
    type: string
    sql: ${TABLE}.batch_id ;;
  }
  dimension: content {
    type: string
    sql: ${TABLE}.content ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: family_hash {
    type: string
    sql: ${TABLE}.family_hash ;;
  }
  dimension: sequence {
    type: number
    sql: ${TABLE}.sequence ;;
  }
  dimension: should_display_on_index {
    type: yesno
    sql: ${TABLE}.should_display_on_index ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
  }
}
