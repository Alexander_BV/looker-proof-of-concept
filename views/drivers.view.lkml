view: drivers {
  sql_table_name: oss_saint_ferdinand_dev.drivers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: device_id {
    type: string
    sql: ${TABLE}.device_id ;;
  }
  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }
  dimension: is_enabled {
    type: yesno
    sql: ${TABLE}.is_enabled ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }
  dimension: license_blood_group {
    type: string
    sql: ${TABLE}.license_blood_group ;;
  }
  dimension: license_category {
    type: string
    sql: ${TABLE}.license_category ;;
  }
  dimension: license_class {
    type: string
    sql: ${TABLE}.license_class ;;
  }
  dimension_group: license_expedition {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.license_expedition_date ;;
  }
  dimension_group: license_expire {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.license_expire_date ;;
  }
  dimension: license_number {
    type: string
    sql: ${TABLE}.license_number ;;
  }
  dimension: license_restrictions {
    type: string
    sql: ${TABLE}.license_restrictions ;;
  }
  dimension: password {
    type: string
    sql: ${TABLE}.password ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	first_name,
	last_name,
	username,
	providers.id,
	providers.name,
	driver_attachments.count,
	incidents.count,
	interval_transport_status.count,
	payments.count,
	postulations.count,
	spents.count,
	subsidiary_driver.count,
	tracking.count,
	trip_events.count,
	trip_transport_has_drivers.count,
	vehicles.count
	]
  }

}
