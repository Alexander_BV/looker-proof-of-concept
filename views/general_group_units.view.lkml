view: general_group_units {
  sql_table_name: oss_saint_ferdinand_dev.general_group_units ;;

  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: general_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.general_group_id ;;
  }
  dimension: measurement_unit_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.measurement_unit_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [measurement_units.id, measurement_units.name, general_group.id, general_group.name]
  }
}
