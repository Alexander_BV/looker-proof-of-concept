view: layouts {
  sql_table_name: oss_saint_ferdinand_dev.layouts ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }
  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }
  dimension: sections {
    type: string
    sql: ${TABLE}.sections ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
