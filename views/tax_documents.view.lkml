view: tax_documents {
  sql_table_name: oss_saint_ferdinand_dev.tax_documents ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: biller_document_number {
    type: string
    sql: ${TABLE}.biller_document_number ;;
  }
  dimension: content {
    type: string
    sql: ${TABLE}.content ;;
  }
  dimension: correlative {
    type: number
    sql: ${TABLE}.correlative ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: entity_id {
    type: number
    sql: ${TABLE}.entity_id ;;
  }
  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }
  dimension: external_id {
    type: string
    sql: ${TABLE}.external_id ;;
  }
  dimension: flag_s3_pdf {
    type: yesno
    sql: ${TABLE}.flag_s3_pdf ;;
  }
  dimension: flag_s3_xml {
    type: yesno
    sql: ${TABLE}.flag_s3_xml ;;
  }
  dimension: flag_s3_xml_cdr {
    type: yesno
    sql: ${TABLE}.flag_s3_xml_cdr ;;
  }
  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }
  dimension: response_api {
    type: string
    sql: ${TABLE}.response_api ;;
  }
  dimension: serie {
    type: string
    sql: ${TABLE}.serie ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_creator_id {
    type: number
    sql: ${TABLE}.user_creator_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
