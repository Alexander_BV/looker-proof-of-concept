view: filesystem_disks {
  sql_table_name: oss_saint_ferdinand_dev.filesystem_disks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: bucket {
    type: string
    sql: ${TABLE}.bucket ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: driver {
    type: string
    sql: ${TABLE}.driver ;;
  }
  dimension: endpoint {
    type: string
    sql: ${TABLE}.endpoint ;;
  }
  dimension: host {
    type: string
    sql: ${TABLE}.host ;;
  }
  dimension: is_active {
    type: yesno
    sql: ${TABLE}.is_active ;;
  }
  dimension: key {
    type: string
    sql: ${TABLE}.`key` ;;
  }
  dimension: passive {
    type: yesno
    sql: ${TABLE}.passive ;;
  }
  dimension: passphrase {
    type: string
    sql: ${TABLE}.passphrase ;;
  }
  dimension: password {
    type: string
    sql: ${TABLE}.password ;;
  }
  dimension: port {
    type: number
    sql: ${TABLE}.port ;;
  }
  dimension: private_key {
    type: string
    sql: ${TABLE}.private_key ;;
  }
  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
  }
  dimension: root {
    type: string
    sql: ${TABLE}.root ;;
  }
  dimension: secret {
    type: string
    sql: ${TABLE}.secret ;;
  }
  dimension: ssl {
    type: yesno
    sql: ${TABLE}.`ssl` ;;
  }
  dimension: timeout {
    type: number
    sql: ${TABLE}.timeout ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }
  dimension: use_path_style_endpoint {
    type: yesno
    sql: ${TABLE}.use_path_style_endpoint ;;
  }
  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, username, filesystem_automation_disks.count]
  }
}
