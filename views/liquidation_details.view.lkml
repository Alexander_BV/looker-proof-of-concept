view: liquidation_details {
  sql_table_name: oss_saint_ferdinand_dev.liquidation_details ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: chargeable_id {
    type: number
    sql: ${TABLE}.chargeable_id ;;
  }
  dimension: chargeable_type {
    type: string
    sql: ${TABLE}.chargeable_type ;;
  }
  dimension: concept {
    type: string
    sql: ${TABLE}.concept ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: exchange_rate {
    type: number
    sql: ${TABLE}.exchange_rate ;;
  }
  dimension: liquidation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.liquidation_id ;;
  }
  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }
  dimension: source_amount {
    type: number
    sql: ${TABLE}.source_amount ;;
  }
  dimension: source_currency_id {
    type: number
    sql: ${TABLE}.source_currency_id ;;
  }
  dimension: target_amount {
    type: number
    sql: ${TABLE}.target_amount ;;
  }
  dimension: target_currency_id {
    type: number
    sql: ${TABLE}.target_currency_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, liquidations.id]
  }
}
