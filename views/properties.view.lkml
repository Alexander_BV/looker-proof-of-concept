view: properties {
  sql_table_name: oss_saint_ferdinand_dev.properties ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: config {
    type: string
    sql: ${TABLE}.config ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: property_group_id {
    type: number
    sql: ${TABLE}.property_group_id ;;
  }
  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }
  dimension: required {
    type: yesno
    sql: ${TABLE}.required ;;
  }
  dimension: rules {
    type: string
    sql: ${TABLE}.rules ;;
  }
  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
    drill_fields: [id]
  }
}
