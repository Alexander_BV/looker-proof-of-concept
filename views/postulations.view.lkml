view: postulations {
  sql_table_name: oss_saint_ferdinand_dev.postulations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: is_confirmed_awarder {
    type: yesno
    sql: ${TABLE}.is_confirmed_awarder ;;
  }
  dimension: is_confirmed_postulant {
    type: yesno
    sql: ${TABLE}.is_confirmed_postulant ;;
  }
  dimension: price_offered {
    type: number
    sql: ${TABLE}.price_offered ;;
  }
  dimension: provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.provider_id ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: trip_proposal_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_proposal_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	requirements.id,
	providers.id,
	providers.name,
	users.id,
	users.first_name,
	users.last_name,
	vehicles.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	trip_proposals.id,
	trip_transports.count
	]
  }

}
