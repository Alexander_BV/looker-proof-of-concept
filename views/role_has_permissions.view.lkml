view: role_has_permissions {
  sql_table_name: oss_saint_ferdinand_dev.role_has_permissions ;;

  dimension: permission_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.permission_id ;;
  }
  dimension: role_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.role_id ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	permissions.id,
	permissions.name,
	permissions.guard_name,
	roles.id,
	roles.name,
	roles.guard_name
	]
  }

}
