view: custom_properties {
  sql_table_name: oss_saint_ferdinand_dev.custom_properties ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: default_value {
    type: string
    sql: ${TABLE}.default_value ;;
  }
  dimension: group {
    type: string
    sql: ${TABLE}.`group` ;;
  }
  dimension: input_type {
    type: string
    sql: ${TABLE}.input_type ;;
  }
  dimension: is_active {
    type: yesno
    sql: ${TABLE}.is_active ;;
  }
  dimension: is_required {
    type: yesno
    sql: ${TABLE}.is_required ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: options {
    type: string
    sql: ${TABLE}.options ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
