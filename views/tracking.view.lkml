view: tracking {
  sql_table_name: oss_saint_ferdinand_dev.tracking ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension_group: datetime {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.datetime ;;
  }
  dimension: distance_to_end_location {
    type: number
    sql: ${TABLE}.distance_to_end_location ;;
  }
  dimension: document {
    type: string
    sql: ${TABLE}.document ;;
  }
  dimension: driver_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.driver_id ;;
  }
  dimension: geo_lat {
    type: number
    sql: ${TABLE}.geo_lat ;;
  }
  dimension: geo_lng {
    type: number
    sql: ${TABLE}.geo_lng ;;
  }
  dimension: hash {
    type: string
    sql: ${TABLE}.hash ;;
  }
  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: phone_data {
    type: string
    sql: ${TABLE}.phone_data ;;
  }
  dimension: rating {
    type: number
    sql: ${TABLE}.rating ;;
  }
  dimension: signature {
    type: string
    sql: ${TABLE}.signature ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: status_code {
    type: string
    sql: ${TABLE}.status_code ;;
  }
  dimension: status_id {
    type: number
    sql: ${TABLE}.status_id ;;
  }
  dimension: sub_status {
    type: string
    sql: ${TABLE}.sub_status ;;
  }
  dimension: trackable_id {
    type: number
    sql: ${TABLE}.trackable_id ;;
  }
  dimension: trackable_type {
    type: string
    sql: ${TABLE}.trackable_type ;;
  }
  dimension: trip_interval_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_interval_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	trip_intervals.id,
	trip_transports.id,
	drivers.id,
	drivers.first_name,
	drivers.last_name,
	drivers.username,
	locations.id,
	locations.name
	]
  }

}
