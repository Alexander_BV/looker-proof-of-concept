view: invoices_orders {
  sql_table_name: oss_saint_ferdinand_dev.invoices_orders ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: invoice_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.invoice_id ;;
  }
  dimension: order_id {
    type: number
    sql: ${TABLE}.order_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, invoices.id]
  }
}
