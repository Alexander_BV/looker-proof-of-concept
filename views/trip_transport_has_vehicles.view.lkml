view: trip_transport_has_vehicles {
  sql_table_name: oss_saint_ferdinand_dev.trip_transport_has_vehicles ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: vehicle_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vehicle_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, vehicles.id, trip_transports.id]
  }
}
