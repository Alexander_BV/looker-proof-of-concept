view: filesystem_automation_disks {
  sql_table_name: oss_saint_ferdinand_dev.filesystem_automation_disks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: callback {
    type: string
    sql: ${TABLE}.callback ;;
  }
  dimension: filesystem_automation_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.filesystem_automation_id ;;
  }
  dimension: filesystem_disk_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.filesystem_disk_id ;;
  }
  dimension: is_active {
    type: yesno
    sql: ${TABLE}.is_active ;;
  }
  dimension: storage_path {
    type: string
    sql: ${TABLE}.storage_path ;;
  }
  dimension: template_filename {
    type: string
    sql: ${TABLE}.template_filename ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	template_filename,
	filesystem_disks.id,
	filesystem_disks.username,
	filesystem_automations.id,
	filesystem_automations.name,
	filesystem_automation_disk_logs.count
	]
  }

}
