view: transmitters {
  sql_table_name: oss_saint_ferdinand_dev.transmitters ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }
  dimension: business_name {
    type: string
    sql: ${TABLE}.business_name ;;
  }
  dimension: cert_uuid {
    type: string
    sql: ${TABLE}.cert_uuid ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: fiscal_regime {
    type: string
    sql: ${TABLE}.fiscal_regime ;;
  }
  dimension: is_default {
    type: yesno
    sql: ${TABLE}.is_default ;;
  }
  dimension: logo {
    type: string
    sql: ${TABLE}.logo ;;
  }
  dimension: pass_phrase {
    type: string
    sql: ${TABLE}.pass_phrase ;;
  }
  dimension: postal_code {
    type: string
    sql: ${TABLE}.postal_code ;;
  }
  dimension: rfc {
    type: string
    sql: ${TABLE}.rfc ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, business_name, invoices.count, series_folios.count]
  }
}
