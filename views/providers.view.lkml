view: providers {
  sql_table_name: oss_saint_ferdinand_dev.providers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: commercial_representative {
    type: string
    sql: ${TABLE}.commercial_representative ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: document_number {
    type: string
    sql: ${TABLE}.document_number ;;
  }
  dimension: document_type_code {
    type: string
    sql: ${TABLE}.document_type_code ;;
  }
  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: is_system_property {
    type: yesno
    sql: ${TABLE}.is_system_property ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: phone {
    type: string
    sql: ${TABLE}.phone ;;
  }
  dimension: provider_type_id {
    type: number
    sql: ${TABLE}.provider_type_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	name,
	drivers.count,
	postulations.count,
	requirements.count,
	tire_retreads.count,
	trip_credits.count,
	trip_proposals.count,
	trip_transports.count,
	users.count,
	vehicle_has_providers.count,
	vehicles.count
	]
  }

}
