view: invoices {
  sql_table_name: oss_saint_ferdinand_dev.invoices ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: cancellation_status {
    type: string
    sql: ${TABLE}.cancellation_status ;;
  }
  dimension: client_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.client_id ;;
  }
  dimension: content {
    type: string
    sql: ${TABLE}.content ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_user_id {
    type: number
    sql: ${TABLE}.creator_user_id ;;
  }
  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }
  dimension: emitter {
    type: string
    sql: ${TABLE}.emitter ;;
  }
  dimension_group: expedition {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.expedition_date ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: folio {
    type: string
    sql: ${TABLE}.folio ;;
  }
  dimension: is_cancelled {
    type: yesno
    sql: ${TABLE}.is_cancelled ;;
  }
  dimension: serie {
    type: string
    sql: ${TABLE}.serie ;;
  }
  dimension: taxes {
    type: number
    sql: ${TABLE}.taxes ;;
  }
  dimension: tc {
    type: number
    sql: ${TABLE}.tc ;;
  }
  dimension: total {
    type: number
    sql: ${TABLE}.total ;;
  }
  dimension: transmitter_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.transmitter_id ;;
  }
  dimension: trip_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_id ;;
  }
  dimension: trip_transport_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trip_transport_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: validation_error {
    type: string
    sql: ${TABLE}.validation_error ;;
  }
  dimension: validation_status {
    type: string
    sql: ${TABLE}.validation_status ;;
  }
  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
	id,
	clients.id,
	clients.name,
	trips.id,
	transmitters.id,
	transmitters.business_name,
	trip_transports.id,
	invoices_orders.count
	]
  }

}
