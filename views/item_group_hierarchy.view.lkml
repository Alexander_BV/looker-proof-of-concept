view: item_group_hierarchy {
  sql_table_name: oss_saint_ferdinand_dev.item_group_hierarchy ;;

  dimension: children_quantity {
    type: number
    sql: ${TABLE}.children_quantity ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: items_group_children_id {
    type: number
    sql: ${TABLE}.items_group_children_id ;;
  }
  dimension: items_group_parent_id {
    type: number
    sql: ${TABLE}.items_group_parent_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  measure: count {
    type: count
  }
}
