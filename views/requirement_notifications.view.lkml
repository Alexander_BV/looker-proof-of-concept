view: requirement_notifications {
  sql_table_name: oss_saint_ferdinand_dev.requirement_notifications ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: cellphone {
    type: string
    sql: ${TABLE}.cellphone ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }
  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }
  dimension: notify_events {
    type: yesno
    sql: ${TABLE}.notify_events ;;
  }
  dimension: notify_geofence {
    type: yesno
    sql: ${TABLE}.notify_geofence ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: workflow_status_codes {
    type: string
    sql: ${TABLE}.workflow_status_codes ;;
  }
  measure: count {
    type: count
    drill_fields: [id, name, requirements.id]
  }
}
