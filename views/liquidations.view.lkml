view: liquidations {
  sql_table_name: oss_saint_ferdinand_dev.liquidations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: avz_uuid {
    type: string
    sql: ${TABLE}.avz_uuid ;;
  }
  dimension_group: cancellation {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.cancellation_date ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: creator_user_id {
    type: number
    sql: ${TABLE}.creator_user_id ;;
  }
  dimension: currency_id {
    type: number
    sql: ${TABLE}.currency_id ;;
  }
  dimension_group: date {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.date ;;
  }
  dimension: extra_charge_amount {
    type: number
    sql: ${TABLE}.extra_charge_amount ;;
  }
  dimension: extra_data {
    type: string
    sql: ${TABLE}.extra_data ;;
  }
  dimension: is_valid {
    type: yesno
    sql: ${TABLE}.is_valid ;;
  }
  dimension: liquidable_id {
    type: number
    sql: ${TABLE}.liquidable_id ;;
  }
  dimension: liquidable_type {
    type: string
    sql: ${TABLE}.liquidable_type ;;
  }
  dimension: metadata {
    type: string
    sql: ${TABLE}.metadata ;;
  }
  dimension: reason_cancellation_id {
    type: number
    sql: ${TABLE}.reason_cancellation_id ;;
  }
  dimension: requirement_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.requirement_id ;;
  }
  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: subtotal {
    type: number
    sql: ${TABLE}.subtotal ;;
  }
  dimension: total {
    type: number
    sql: ${TABLE}.total ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_cancellation_id {
    type: number
    sql: ${TABLE}.user_cancellation_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, requirements.id, extra_charges.count, liquidation_details.count, liquidation_trips.count]
  }
}
