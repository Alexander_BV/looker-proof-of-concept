view: config_email {
  sql_table_name: oss_saint_ferdinand_dev.config_email ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: delivered {
    type: string
    sql: ${TABLE}.delivered ;;
  }
  dimension: emitter_name {
    type: string
    sql: ${TABLE}.emitter_name ;;
  }
  dimension: logo {
    type: string
    sql: ${TABLE}.logo ;;
  }
  dimension: on_my_way {
    type: string
    sql: ${TABLE}.on_my_way ;;
  }
  dimension: partial {
    type: string
    sql: ${TABLE}.partial ;;
  }
  dimension: reply_to {
    type: string
    sql: ${TABLE}.reply_to ;;
  }
  dimension: undelivered {
    type: string
    sql: ${TABLE}.undelivered ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  dimension: widget_style {
    type: string
    sql: ${TABLE}.widget_style ;;
  }
  measure: count {
    type: count
    drill_fields: [id, emitter_name]
  }
}
