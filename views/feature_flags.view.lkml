view: feature_flags {
  sql_table_name: oss_saint_ferdinand_dev.feature_flags ;;

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension: is_enabled {
    type: yesno
    sql: ${TABLE}.is_enabled ;;
  }
  measure: count {
    type: count
  }
}
