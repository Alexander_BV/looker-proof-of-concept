view: attachments {
  sql_table_name: oss_saint_ferdinand_dev.attachments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: avz_uuid {
    type: string
    sql: ${TABLE}.avz_uuid ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: entity_id {
    type: number
    sql: ${TABLE}.entity_id ;;
  }
  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }
  dimension: extension {
    type: string
    sql: ${TABLE}.extension ;;
  }
  dimension: mimetype {
    type: string
    sql: ${TABLE}.mimetype ;;
  }
  dimension: original_name {
    type: string
    sql: ${TABLE}.original_name ;;
  }
  dimension: size {
    type: number
    sql: ${TABLE}.size ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, original_name, driver_attachments.count, shared_documents.count]
  }
}
