view: lead_time_has_vehicle_types {
  sql_table_name: oss_saint_ferdinand_dev.lead_time_has_vehicle_types ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: lead_time_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.lead_time_id ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: vehicle_type_id {
    type: number
    sql: ${TABLE}.vehicle_type_id ;;
  }
  measure: count {
    type: count
    drill_fields: [id, lead_times.id]
  }
}
