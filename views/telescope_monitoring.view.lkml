view: telescope_monitoring {
  sql_table_name: oss_saint_ferdinand_dev.telescope_monitoring ;;

  dimension: tag {
    type: string
    sql: ${TABLE}.tag ;;
  }
  measure: count {
    type: count
  }
}
