view: tires_history {
  sql_table_name: oss_saint_ferdinand_dev.tires_history ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }
  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }
  dimension_group: created {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.created_at ;;
  }
  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }
  dimension: odometer {
    type: number
    sql: ${TABLE}.odometer ;;
  }
  dimension: rtd {
    type: number
    sql: ${TABLE}.rtd ;;
  }
  dimension: tire_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.tire_id ;;
  }
  dimension: tire_rolling_id {
    type: number
    sql: ${TABLE}.tire_rolling_id ;;
  }
  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }
  dimension_group: updated {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.updated_at ;;
  }
  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }
  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }
  measure: count {
    type: count
    drill_fields: [id, users.id, users.first_name, users.last_name, tires.id]
  }
}
